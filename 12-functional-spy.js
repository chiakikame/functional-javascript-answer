'use strict';

function Spy(target, method) {
  const originalMethod = target[method];
  const counter = {count: 0};
  target[method] = function() {
    counter.count += 1;
    return originalMethod.apply(target, arguments);
  }
  return counter;
}

module.exports = Spy;
