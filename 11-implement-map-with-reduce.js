module.exports = function arraymap(arr, cb, thisArg) {
  const _thisArg = thisArg ? thisArg : null;
  
  return arr.reduce((acc, elem, idx, arrBody) => [...acc, cb.call(_thisArg, elem, idx, arrBody)], []);
};
