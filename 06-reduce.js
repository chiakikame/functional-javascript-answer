function countWords(list) {
  return list.reduce((acc, elem) => {
    acc[elem] = acc[elem] ? acc[elem] + 1 : 1;
    return acc;
  }, {});
}

module.exports = countWords;
