// fn: acc elem idx arr
function reduce(arr, fn, init) {
  return _reduce(arr, fn, init, 0);
}

function _reduce(arr, fn, acc, idx) {
  const accNew = fn(acc, arr[idx], idx, arr);
  if (idx + 1 >= arr.length) {
    return accNew;
  } else {
    return _reduce(arr, fn, accNew, idx + 1);
  }
}

module.exports = reduce;
