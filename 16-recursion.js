function _getDependencies(tree) {
  if (tree && tree.dependencies) {
    const deps = tree.dependencies;
    const items = Object.keys(deps).map(depName => {
      return [`${depName}@${deps[depName].version}`, ..._getDependencies(deps[depName])];
    });
    return Array.prototype.concat.apply([], items);
  } else {
    return [];
  }
}

module.exports = function getDependencies(tree) {
  const rawArray = _getDependencies(tree);
  return Array.from(new Set(rawArray)).sort();
}
