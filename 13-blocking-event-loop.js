'use strict';

function repeat(op, num) {
  if (num <= 0) return;
  op();
  setTimeout( () => {
    repeat(op, num - 1);
  }, 10);
}

module.exports = repeat;
