# Notes

All other parts of this workshop are easy to grasp if you have played with some functional programming languages or languages with similar concepts (e.g. C# LINQ, python, kotlin etc.).

## Trampoline

Trampoline is a technique used to tail-call optimization. We can apply it when you wish to avoid stack overflow on a stack-based function call system.

The trampoline system contains 3 major components

* Starter: call the trampoline function with an initial state.
* Trampoline function: a loop which determine if the system should issue the next call.
* Main computation function. The computation function should return a state for the trampoline.

## Make a function without `function` keyword

The following are copied from the workshop exercise description.

> * Every JavaScript Function inherits methods such as call, apply and bind from the object `Function.prototype`.
> * Function#call executes the value of `this` when it is invoked.  Inside `someFunction.call()`, the value of `this` will be `someFunction`.
> * Function.call itself is a function thus it inherits from `Function.prototype`

> ```js
> function myFunction() {
>   console.log('called my function')
> }
>
> Function.prototype.call.call(myFunction) // => "called my function"
> ```

And, the following are copied from the answer part of the workshop.

> The value of `this` in Function.call is the function that will be executed.
> Bind returns a new function with the value of `this` fixed to whatever was passed as its first argument.
> Every function 'inherits' from Function.prototype, thus every function, including call, apply and bind have the methods call apply and bind.
