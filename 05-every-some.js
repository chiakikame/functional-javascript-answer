function checkUsersValid(goodUsers) {
  const ids = goodUsers.map(x => x.id);
  return function allUsersValid(submittedUsers) {
    const idss = submittedUsers.map(x => x.id);
    const result = idss.every(id => ids.indexOf(id) != -1);
    return result;
  };
}

module.exports = checkUsersValid;
