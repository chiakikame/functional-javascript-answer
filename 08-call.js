function duckCount() {
  return Array.prototype.reduce.call(arguments, (acc, elem) => {
    return Object.prototype.hasOwnProperty.call(elem, 'quack') ? acc + 1 : acc;
  }, 0);
}

module.exports = duckCount;
