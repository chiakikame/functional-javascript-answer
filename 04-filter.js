function getShourtMessages(msgs) {
  return msgs.filter(msg =>
    msg.message.length < 50
  ).map(msg => msg.message);
}

module.exports = getShourtMessages;
