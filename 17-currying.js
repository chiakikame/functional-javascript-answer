function curryN(fn, n) {
  const curryCount = n ? n : fn.length;
  if (curryCount <= 1) {
    return fn;
  }
  
  const ctx = this;
  
  return function(v) {
    return curryN(fn.bind(ctx, v), n - 1);
  }
}

module.exports = curryN;
