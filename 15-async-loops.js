function loadUsers(userIds, load, done) {
  const users = [];
  let finished = 0;
  
  userIds.forEach((elem, idx) => {
    load(elem, (v) => {
      users[idx] = v;
      finished += 1;
      if (finished === userIds.length) {
        done(users.filter(elem => elem ? true : false));
      }
    });
  });
}

module.exports = loadUsers;
