function logger(title) {
  return function() {
    const args = [title, ... arguments];
    console.log.apply(null, args);
  }
}

module.exports = logger;
