// http://stackoverflow.com/questions/189725/what-is-a-trampoline-function
// The function returns a state, and the state will be interpreted in the trampoline's loop.

function repeat(operation, num) {
  // Modify this so it doesn't cause a stack overflow!
  if (num <= 0) return {};
  operation();
  return {run: repeat, args: [operation, num - 1]};
}

function trampoline(fn) {
  while(fn && fn.run) {
    fn = fn.run.apply(null, fn.args);
  }
}

module.exports = function(operation, num) {
  // You probably want to call your trampoline here!
  return trampoline( {run: repeat, args: [operation, num] } );
}
